<?php

/**
 * Plugin Name: BTCUSD Converter
 * Description: Enable to use the widget.
 * Version: 0.9a
 * Author:  Wojciech Sobiak
 * License: MIT
 */
class BTC_price_calc extends WP_Widget {

    function __construct() {
        $widget_options = array(
            'classname' => 'BTC_price_calc',
            'description' => 'BTC to USD calculator.',
        );
        parent::__construct('BTC_price_calc', 'BTC to USD calculator.', $widget_options);
    }

    function widget($args, $instance) {


        $url = "https://blockchain.info/pl/ticker";

        $json = file_get_contents($url);
        $data = json_decode($json);

        $rateUSD = $data->USD->last;


        $BTC = $rateUSD;
        $amountBTC = $_POST["amountBTC"];
        ?>
        <h3> Aktualny kurs BTC<br /><?php echo $BTC ?> USD</h3>
        <form action="" method="POST">
            <p>Wpisz ilość BTC jaką posiadasz: </p>
            <input id="amountBTC" type="text" name="amountBTC" /><br />
            <input type="submit" value="Wylicz USD" />
        </form>

        <?php
        $result = $BTC * $amountBTC;
        ?>


        <h3>Posiadasz: <?php echo $result ?> USD</h3>

        <?php
    }

}

function register_BTC_price_calc() {
    register_widget('BTC_price_calc');
}

add_action('widgets_init', 'register_BTC_price_calc');
