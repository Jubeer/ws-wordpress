<?php

/**
 * Plugin Name: Replace "BTC" related word.
 * Description: Enable to replace all BTC words in all pages.
 * Version: 0.9b
 * Author:  Wojciech Sobiak
 * License: MIT
 */

   add_filter("the_content","replacewithimage");

    function replacewithimage() {

      $btc_img = "<img src='http://wsobiak.pl/wp-content/plugins/replacewithimage/qazwsx.png'>";
      $content = get_the_content();
      $toReplaceWords = ['btc','BTC','Bitcoina','Bitcoin','Bitcoiny'];
      $replace = $btc_img;


      for ($i = 0; $i < count($toReplaceWords); $i++) {
      $content = str_ireplace($toReplaceWords[$i], $replace, $content);
      }
      return $content;

    }
