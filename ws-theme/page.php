<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="row well">
            <div class="col-md-12">
                <div class="panel panel-warning">
                    <div class="panel-heading text-center"><h1><?php the_title(); ?></h1></div>
                    <div class="panel-body"> 
                        <p><?php the_content(); ?></p>
                    </div>
                </div>
            </div>
        </div>        <?php endwhile;
else: ?>
    <p><?php _e('Brak danych'); ?></p>
<?php endif; ?>                
<?php get_footer(); ?>
