<?php get_header(); ?>

<div class="col-md-8">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="row well">

                <div class="panel panel-warning">
                    <div class="panel-heading text-center"><h3><?php the_title(); ?></h3></div>
                    <div class="panel-body">
                        <div class="col-md-4">       
                            <?php if (has_post_thumbnail()): ?>

                                <a href="<?php the_post_thumbnail_url(); ?>" target="_blank">                   
                                    <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive responsive--full']); ?>
                                </a>

                            <?php else: ?>
                                <img class="img-responsive" src="http://placehold.it/600x300" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="col-md-8"> 
                            <h4>Autor: <?php the_author(); ?></h4>
                            <h6>Data wpisu: <?php the_time('F jS, Y'); ?></h6>
                            <p><?php
                                $content = get_the_content();
                                echo substr($content, 0, 300);
                                echo "...";
                                ?></p>                
                            <a class="btn btn-primary" href="<?php the_permalink(); ?>">Czytaj dalej... <span class="glyphicon glyphicon-chevron-right"></span></a>                
                        </div>
                    </div>
                </div>
            </div>    
            <hr>
        <?php endwhile;
    else: ?>
        <p><?php _e('Nie ma wpisów do wyświetlenia...'); ?></p>
<?php endif; ?>
</div>
<div class="col-md-4">
<?php get_sidebar(); ?> 
</div>        

<?php get_footer(); ?>