<?php get_header(); ?>
<div class="row well">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="panel panel-warning">
                <div class="panel-heading text-center"><h1><?php the_title(); ?></h1></div>
                <div class="panel-body">         
                    <p><em><?php the_time('l, F jS, Y'); ?></em></p>                
                    <?php if (has_post_thumbnail()): ?>
                        <a href="<?php the_post_thumbnail_url(); ?>" target="_blank">                   
                            <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive responsive--full']); ?>
                        </a>
                    <?php else: ?>                 
                        <img class="img-responsive" src="http://placehold.it/600x300" alt="">
                    <?php endif; ?>
                    <hr>
                    <?php the_content(); ?>
                    <hr>
                    <?php comments_template(); ?>

                <?php
                endwhile;
            else:
                ?>
                <p><?php _e('Podstrona nie istnieje. Error 404.'); ?></p>
<?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
